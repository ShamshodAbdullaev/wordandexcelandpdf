package uz.pdp;

import uz.pdp.servise.PhotoServise;

public class Main {
    public static void main(String[] args) {
        PhotoServise.readFromUrl();
        PhotoServise.writeUrlToWord("e:/photos.docx");
        PhotoServise.writeUrlToExcel("e:/photos.xlsx");
        PhotoServise.writeUrlToPdf("e:/photos.pdf");
    }
}
