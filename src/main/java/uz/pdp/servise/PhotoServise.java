package uz.pdp.servise;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.itextpdf.text.*;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.*;
import uz.pdp.model.Photo;

import java.io.*;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import static org.apache.poi.xwpf.usermodel.ParagraphAlignment.CENTER;

public class PhotoServise {
    public static ArrayList<Photo>photos = new ArrayList<Photo>();


    public static void writeUrlToWord(String urlWordFile){
        OutputStream outputStream = null;
        try {
            File wordFile = new File("e:/photos.docx");
            outputStream = new FileOutputStream(wordFile);

            XWPFDocument document = new XWPFDocument();
            XWPFParagraph paragraph = document.createParagraph();
            paragraph.setAlignment(CENTER);
            XWPFRun run =paragraph.createRun();
            run.setText("photos");
            run.setCapitalized(true);
            run.setFontSize(32);
            run.setFontFamily("Times New Roman");

            XWPFTable table= document.createTable();
            XWPFTableRow row = table.getRow(0);
            XWPFTableCell cell = row.getCell(0);
            cell.setText("Id");
            cell = row.createCell();
            cell.setText("AlbumId");
            cell = row.createCell();
            cell.setText("Title");
            cell = row.createCell();
            cell.setText("Url");
            cell = row.createCell();
            cell.setText("ThumbnailUrl");

            for (Photo photo : photos) {
                row = table.createRow();

                cell =row.getCell(0);
                cell.setText(String.valueOf(photo.getId()));

                cell =row.getCell(1);
                cell.setText(String.valueOf(photo.getAlbumId()));

                cell = row.getCell(2);
                cell.setText(photo.getTitle());

                cell = row.getCell(3);
                cell.setText(photo.getUrl());

                cell = row.getCell(4);
                cell.setText(photo.getThumbnailUrl());
            }


            document.write(outputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void writeUrlToExcel(String urlExcelFile){
        OutputStream outputStream = null;
        try {
            File excelFile = new File("e:/photos.xlsx");
            outputStream = new FileOutputStream(excelFile);
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.createSheet("Photos");
            XSSFRow row = sheet.createRow(0);
            XSSFCell cell = row.createCell(0);
            cell.setCellValue("PHOTOS");

            row = sheet.createRow(1);
            cell = row.createCell(0);
            cell.setCellValue("Id");
            cell = row.createCell(1);
            cell.setCellValue("AlbumId");
            cell = row.createCell(2);
            cell.setCellValue("Title");
            cell = row.createCell(3);
            cell.setCellValue("Url");
            cell = row.createCell(4);
            cell.setCellValue("ThumbnailUrl");

            for (int i =0; i < photos.size(); i++) {
                row = sheet.createRow(i+2);

                cell = row.createCell(0);
                cell.setCellValue(String.valueOf(photos.get(i).getId()));

                cell = row.createCell(1);
                cell.setCellValue(String.valueOf(photos.get(i).getAlbumId()));

                cell = row.createCell(2);
                cell.setCellValue(photos.get(i).getTitle());

                cell = row.createCell(3);
                cell.setCellValue(photos.get(i).getUrl());

                cell = row.createCell(4);
                cell.setCellValue(photos.get(i).getThumbnailUrl());
            }



            workbook.write(outputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void writeUrlToPdf(String urlPdfFile){
        File pdfFile = new File("e:/photos.pdf");
        OutputStream outputStream = null;
        Document document = new Document();
        try {
            outputStream = new FileOutputStream(pdfFile);
            PdfWriter.getInstance(document,outputStream);
            document.open();

            Font font = new Font(Font.FontFamily.TIMES_ROMAN,20,Font.NORMAL, BaseColor.DARK_GRAY);
            Paragraph paragraph = new Paragraph("PHOTOS",font);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            paragraph.setSpacingAfter(15f);
            document.add(paragraph);

            PdfPTable table = new PdfPTable(5);
            table.setWidthPercentage(100);
            float[] width = {5,10,25,30,30};
            table.setWidths(width);
            PdfPCell cellId = new PdfPCell(new Paragraph("Id"));
            table.addCell(cellId);
            PdfPCell cellAlbumId = new PdfPCell(new Paragraph("AlbumId"));
            table.addCell(cellAlbumId);
            PdfPCell cellTitle = new PdfPCell(new Paragraph("Title"));
            table.addCell(cellTitle);
            PdfPCell cellUrl = new PdfPCell(new Paragraph("Url"));
            table.addCell(cellUrl);
            PdfPCell cellThumbnailUrl = new PdfPCell(new Paragraph("ThumbnailUrl"));
            table.addCell(cellThumbnailUrl);

            for (Photo photo : photos) {
                table.addCell(String.valueOf(photo.getId()));
                table.addCell(String.valueOf(photo.getAlbumId()));
                table.addCell(photo.getTitle());
                table.addCell(photo.getUrl());
                table.addCell(photo.getThumbnailUrl());
            }
            document.add(table);



            document.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }


    public static void readFromUrl(){
    Gson gson = new Gson();

        try {
        URL url = new URL("https://jsonplaceholder.typicode.com/photos");

        URLConnection urlConnection = url.openConnection();
        BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
//            File file = new File("d:/photos.json");
//            BufferedReader reader = new BufferedReader(new FileReader(file));
        Type type = new TypeToken<ArrayList<Photo>>(){}.getType();
        photos = gson.fromJson(reader,type);


    } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        }

    }
