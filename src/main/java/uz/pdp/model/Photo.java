package uz.pdp.model;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class Photo {
    private Integer id;
    private Integer albumId;
    private String title;
    private String url;
    private String thumbnailUrl;
}
